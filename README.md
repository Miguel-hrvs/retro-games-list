New retro styled games, some made for vintage consoles!

Duckstroma (amiga, msx, zx, nes):
https://narwhal.itch.io/duckstroma

Space gulls:
https://morphcatgames.itch.io/spacegulls

Tobu girl deluxe (gbc): 
https://tangramgames.itch.io/tobu-tobu-girl-deluxe

Super bread box (c64):
https://rgcddev.itch.io/super-bread-box

Sweet hell (msx):
https://donitz.itch.io/sweet-hell

Cocoa and the time machine (zx, msx):
https://minilop.itch.io/cocoa-and-the-time-machine

Lilly's saga (msx):
https://www.msxdev.org/2022/10/05/msxdev22-31-lillys-saga-the-stones-of-evergreen/